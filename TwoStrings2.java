public class TwoStrings2 {
	static void print(String str1, String str2) {
		System.out.print(str1);
		try {
			Thread.sleep(500);
		} catch (InterruptedException ie) {
			// TODO: handle exception
		}
		System.out.println(str2);
	}
}

class PrintStringsThread2 implements Runnable{
	Thread thread;
	String str1, str2;
	public PrintStringsThread2(String str1, String str2) {
		// TODO Auto-generated constructor stub
		this.str1 = str1;
		this.str2 = str2;
		thread = new Thread(this);
		thread.start();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		TwoStrings.print(str1, str2);
	}
}

class TestThread2{
	public static void main(String args[]) {
		new PrintStringsThread2("Hello ", "there.");
		new PrintStringsThread2("How are ", "you?");
		new PrintStringsThread2("Thank you ", "very much!");
	}
}